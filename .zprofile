#!/bin/sh

source ~/.profile

if [[ -z $DISPLAY ]] && [[ $(tty) = /dev/tty1 ]]; then
  # exec startx -- :1
  eval $(/usr/bin/gnome-keyring-daemon --start --components=pkcs11,secrets,ssh)
  export SSH_AUTH_SOCK

  # export QT_QPA_PLATFORM=wayland
  # export QT_QPA_PLATFORMTHEME=qt5ct
  export XDG_CURRENT_DESKTOP=Unity
  export WINIT_HIDPI_FACTOR=1.0
  exec dbus-launch --sh-syntax --exit-with-session sway 1>~/.local/share/sway.log 2>&1
fi
