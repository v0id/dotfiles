# TODO: optimizar plugin de indent
# TODO: cargar plugins dinamicamente bien (preguntar)
# Plugins
source "%val{config}/plugins/plug.kak/rc/plug.kak"

# Force disable kitty integration (I don't like kitty's splits)
# rmhooks global kitty-hooks
# hook global KakBegin .* %{ set global termcmd 'kitty --single-instance sh -c' } # override alacritty

# plug "ul/kak-lsp" do %{
#     cargo install --locked --force --path .
# }

plug "andreyorst/fzf.kak" defer fzf %{
    # Use ripgrep for fzf
    set global fzf_file_command "rg"
    set global fzf_implementation "sk"
    set global fzf_preview false
}
# plug "greenfork/active-window.kak"
plug "andreyorst/plug.kak" noload
# plug "alexherbo2/kakoune-dracula-theme" theme
# plug "lenormf/kakoune-extra" load %{
#     dictcomplete.kak
# }

# plug "TeddyDD/kakoune-selenized" theme
# plug "occivink/kakoune-sudo-write"
plug "alexherbo2/move-line.kak"
# plug "alexherbo2/auto-pairs.kak"

plug "ABuffSeagull/kakoune-vue"

plug "andreyorst/powerline.kak" defer powerline %{
    powerline-theme gruvbox
    set global powerline_format 'git bufname filetype mode_info line_column'
} config %{
    powerline-start
}
# plug 'delapouite/kakoune-buffers'

# colorscheme base16-nord
colorscheme gruvbox

# Use Tab for completion and indentation
hook global InsertCompletionShow .* %{
    try %{
        # this command temporarily removes cursors preceded by whitespace;
        # if there are no cursors left, it raises an error, does not
        # continue to execute the mapping commands, and the error is eaten
        # by the `try` command so no warning appears.
        execute-keys -draft 'h<a-K>\h<ret>'
        map window insert <tab> <c-n>
        map window insert <s-tab> <c-p>
    }
}
hook global InsertCompletionHide .* %{
    unmap window insert <tab> <c-n>
    unmap window insert <s-tab> <c-p>
}

# Show mathching {}
add-highlighter global/ show-matching
# Wrap lines
add-highlighter global/ wrap

add-highlighter global/ number-lines

# add-highlighter global/ show-whitespaces

# Attempt to show git diff
#try %{ add-highlighter global/git-diff flag-lines Default git_diff_flags }
#hook global WinCreate ^[^*]+$ %{ try %{ git show-diff } }

# Underline search matches in all newly created windows
add-highlighter global/ dynregex '%reg{/}' 0:+ub

# Comment with Ctrl+V
map global normal <c-v> ': comment-line<ret>'

# Ctrl+P
map global normal <c-p> ': fzf-mode<ret>f'

# Clear search pattern with Ctrl+/
map global normal <c-a-s> ': reg / ""<ret>'

# Load editorconfig automatically
# hook global WinCreate ^[^*]+$ %{editorconfig-load}

# Code formatting
hook global WinSetOption filetype=(javascript|typescript|css|scss|sass|json|html|svelte|vue) %{
    set window formatcmd "prettier --stdin-filepath=%val{buffile}"
    # set window formatcmd "pnpx prettier --stdin-filepath=%val{buffile}"
    # set window formatcmd "prettier_d_slim --stdin --stdin-filepath %val{buffile}"
    hook buffer BufWritePre .* %{ format }
}

# LSP
eval %sh{kak-lsp --kakoune -s $kak_session}
hook global WinSetOption filetype=(rust|python|go|svelte|css|sass|scss|less|html|json|javascript|typescript) %{
    lsp-enable-window
    lsp-auto-hover-enable
    lsp-inline-diagnostics-enable buffer
    # lsp-inlay-diagnostics-enable buffer
    lsp-auto-hover-insert-mode-disable
    set-option window lsp_hover_anchor true
    # set-face window DiagnosticError black,red+u
    # set-face window DiagnosticWarning black,yellow+u
}
hook global WinSetOption filetype=(rust) %{
    # set window lsp_server_configuration rust.clippy_preference="on"
    set window formatcmd 'rustfmt'
    hook window BufWritePre .* %{
        format-buffer
    }
}
set global lsp_diagnostic_line_error_sign '║'
set global lsp_diagnostic_line_warning_sign '┊'
set global lsp_hover_max_lines 7
hook global KakEnd .* lsp-exit

# Nicer <x> behaviour
def -params 1 extend-line-down %{
    exec "<a-:>%arg{1}X"
}
def -params 1 extend-line-up %{
    exec "<a-:><a-;>%arg{1}K<a-;>"
    try %{
        exec -draft ';<a-K>\n<ret>'
        exec X
    }
    exec '<a-;><a-X>'
}
map global normal x ': extend-line-down %val{count}<ret>'
map global normal X ': extend-line-up %val{count}<ret>'

# Detecting indent
define-command -hidden go-backwards %{
    try %{
        execute-keys -draft 'h<a-h><a-k>\A\h+\z<ret>i<space><esc><lt>'
    }
}
define-command detect-indent %{
    evaluate-commands %sh{ node ~/Projects/detect-indent.kak/index.js "$kak_buffile" }
}
hook global BufCreate .+ detect-indent

# Bar at top
#set global ui_options ncurses_status_on_top=yes
set global ui_options ncurses_set_title=no
# Leave space around selection
set global scrolloff 3,3

map global normal R ': lsp-rename-prompt<ret>'
map global normal <c-x> ': lsp-code-actions<ret>'

# hook global KakBegin .* %{ alias global terminal kitty-terminal }

map global normal "'" ': move-line-below<ret>'
map global normal "<a-'>" ': move-line-above<ret>'

hook global BufCreate .+\.svelte %{
    set-option buffer filetype svelte
}

hook global WinSetOption filetype=svelte %{
    require-module html
    add-highlighter window/svelte ref html
}
hook global WinSetOption filetype=(?!svelte).* %{
    remove-highlighter window/svelte
}
