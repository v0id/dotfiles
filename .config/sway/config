set $mod Mod4

set $left h
set $down j
set $up k
set $right l

set $term alacritty

font JetBrains Mono Regular 11
titlebar_padding 5 1

# output * bg ~/Pictures/Firefox\ Developer\ Edition_wallpaper.png fill

# Display management
set $laptop eDP-1
bindswitch --locked lid:on exec sway-lid-handler --lock
# bindswitch --locked lid:off exec sway-lid-handler
bindsym $mod+F1 output $laptop toggle

exec --no-startup-id {
    swayidle \
        timeout 5 "if ps ax -o cmd | grep '^swaylock'; then; auto-zzzz; fi" \
        timeout 570 "notify-send 'Suspendiendo en 30seg...' -t 30000" \
        timeout 600 'auto-zzzz' \
        before-sleep 'lock'
    mako
    syncthing -no-browser
    mkfifo $SWAYSOCK.wob && tail -f $SWAYSOCK.wob | wob
    xsettingsd # for gtk settings in xwayland apps
    redshift -r -l -34.57:-58.44
    blueman-applet
    # kdeconnect-indicator
    # echo -e "power on\nagent on\ndefault-agent\n" | bluetoothctl
    mpd
    mpDris2
    # ~/.config/waybar/launch
    kanshi
    # killall ~/.local/bin/wristcarev2; ~/.local/bin/wristcarev2
}
exec_always {
    light -I
}

input * {
    tap enabled
    dwt enabled
    click_method clickfinger
    
    repeat_delay 180
    repeat_rate 65

    xkb_layout us
    xkb_variant altgr-intl
    xkb_options caps:escape
}

# input "1:1:AT_Translated_Set_2_keyboard" {
#     xkb_layout us
#     xkb_variant altgr-intl
#     xkb_model chromebook
#     xkb_options caps:escape
# }

input "2:10:TPPS/2_Elan_TrackPoint" pointer_accel -0.3
input "2:7:SynPS/2_Synaptics_TouchPad" {
    pointer_accel 0.5
    natural_scroll enabled
}
input "1133:45078:Bluetooth_Mouse_M336/M337/M535_Mouse" pointer_accel 0.3

input "1452:591:SONiX_USB_DEVICE" {
    xkb_layout us
    xkb_variant velocifire
    xkb_options caps:escape
}

input "1452:591:Velocifire_TKL61WS_Keyboard" {
    xkb_layout us
    xkb_variant velocifire
    xkb_options caps:escape
}

# Moving around:
## Move your focus around
bindsym $mod+$left focus left
bindsym $mod+$down focus down
bindsym $mod+$up focus up
bindsym $mod+$right focus right

## Move the focused window with the same, but add Shift
bindsym $mod+Shift+$left move left
bindsym $mod+Shift+$down move down
bindsym $mod+Shift+$up move up
bindsym $mod+Shift+$right move right

# Workspaces:
## Switch to workspace
bindsym $mod+z workspace 1
bindsym $mod+x workspace 2
bindsym $mod+c workspace 3
bindsym $mod+a workspace 4
bindsym $mod+d workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10
## Move focused container to workspace
bindsym $mod+Shift+z move container to workspace 1
bindsym $mod+Shift+x move container to workspace 2
bindsym $mod+Shift+c move container to workspace 3
bindsym $mod+Shift+a move container to workspace 4
bindsym $mod+Shift+d move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

bindsym $mod+Ctrl+h move workspace to output left
bindsym $mod+Ctrl+j move workspace to output down
bindsym $mod+Ctrl+k move workspace to output up
bindsym $mod+Ctrl+l move workspace to output right

# Keybindings
# bindsym Mod4+F3 exec mpc toggle
## Screenshots
# bindsym Mod4+F5 exec sh -c 'O=~/Pictures/$(date +screenshoot.%s.png); grim -g "$(slurp)" $O; sh -c "wl-copy < "$O'
# bindsym Mod4+Shift+F5 exec bash -c 'grim ~/Pictures/"$(date +screenshoot.%s)"'
bindsym Print exec sh -c 'O=~/Pictures/$(date +screenshoot.%s.png); grim -g "$(slurp)" $O; sh -c "wl-copy < "$O'
bindsym $mod+Print exec bash -c 'grim ~/Pictures/"$(date +screenshoot.%s)"'
## Backlight brightness
# bindsym Mod4+F6 exec light -I && light -U 4 && light -O
# bindsym Mod4+F7 exec light -I && light -A 4 && light -O
bindsym XF86MonBrightnessDown exec sh -c 'light -I; light -U 4; light -O'
bindsym XF86MonBrightnessUp exec sh -c 'light -I; light -A 4; light -O'
## Volume management
# bindsym Mod4+F8 exec pamixer --toggle-mute && ( pamixer --get-mute && echo 0 > $SWAYSOCK.wob ) || pamixer --get-volume > $SWAYSOCK.wob
# bindsym Mod4+F9 exec pamixer -ud 4 && pamixer --get-volume > $SWAYSOCK.wob
# bindsym Mod4+F10 exec pamixer -ui 4 && pamixer --get-volume > $SWAYSOCK.wob
bindsym XF86AudioMute exec pamixer --toggle-mute && ( pamixer --get-mute && echo 0 > $SWAYSOCK.wob ) || pamixer --get-volume > $SWAYSOCK.wob
bindsym XF86AudioLowerVolume exec pamixer -ud 4 && pamixer --get-volume > $SWAYSOCK.wob
bindsym XF86AudioRaiseVolume exec pamixer -ui 4 && pamixer --get-volume > $SWAYSOCK.wob

bindsym XF86AudioPrev exec mpc prev
bindsym XF86AudioPlay exec mpc toggle
bindsym XF86AudioNext exec mpc next

## Locking
bindsym $mod+m exec lock
bindsym $mod+Shift+m exec zzzz
## Execute programs
bindsym $mod+Shift+t exec $term
bindsym $mod+Return exec wofi --show drun,run -i | xargs swaymsg exec --
# bindsym $mod+Return exec dmenu_run
## Quit programs
bindsym $mod+Shift+q kill
## Despite the name, also works for non-floating windows.
floating_modifier $mod normal
## Sway management
bindsym $mod+Ctrl+Shift+r reload
bindsym $mod+Shift+e exec swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your Wayland session.' -b 'Yes, exit sway' 'swaymsg exit'

# Layout stuff:
## $mod+b or $mod+v, for horizontal and vertical splits respectively.
bindsym $mod+b splith
bindsym $mod+v splitv
## Switch the current container between different layout styles
bindsym $mod+q layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

bindsym $mod+f fullscreen
bindsym $mod+p sticky toggle

bindsym $mod+Shift+space floating toggle
bindsym $mod+space focus mode_toggle
## Move focus to the parent container
bindsym $mod+s focus parent
bindsym $mod+Shift+s focus child

# Scratchpad:
## Move the currently focused window to the scratchpad
bindsym $mod+Shift+minus move scratchpad
## Show the next scratchpad window or hide the focused scratchpad window.
bindsym $mod+minus scratchpad show

# Resizing containers:
mode "resize" {
    bindsym $left resize shrink width 10px
    bindsym $down resize grow height 10px
    bindsym $up resize shrink height 10px
    bindsym $right resize grow width 10px

    # Return to default mode
    bindsym Return mode "default"
    bindsym $mod mode "default"
    bindsym Escape mode "default"
}
bindsym $mod+r mode "resize"

default_border pixel 3
smart_borders on
# gaps inner 10
smart_gaps on

# bar swaybar_command ~/.config/waybar/launch
bar {
    font pango:Fantasque Sans Mono, FontAwesome 12
    height 17
    status_padding 0
    position bottom
    status_command ~/.cargo/bin/i3status-rs
    colors {
        separator #666666
        background #222222
        statusline #dddddd

        focused_workspace #cc241d #cc241d #ffffff
        active_workspace #333333 #333333 #ffffff
        inactive_workspace #333333 #333333 #888888
        urgent_workspace #2f343a #900000 #ffffff
    }
}

# Cursor
seat seat0 xcursor_theme Adwaita 24

# App-specific config
for_window [app_id="firefox"] {
    inhibit_idle fullscreen
    title_format Firefox
    border pixel 3
}
for_window [class="mpv"] inhibit_idle fullscreen
for_window [app_id="mpv"] inhibit_idle fullscreen

assign [app_id="firefox"] 1
# assign [class="Firefox"] 1
assign [app_id="dino"] 4
assign [app_id="telegramdesktop"] 4
assign [app_id="org.keepassxc.KeePassXC"] 5
assign [class="Deezloader Remix"] 5
assign [class="Claws-mail"] 5

for_window [app_id="Notas"] {
    floating enable
    resize set width 500px
    resize set height 300px
}

for_window [title="Picture-in-Picture"] {
    sticky enable
    floating enable
}

# A loose attempt at using gruvbox
set $bg-color #3c3836
set $fg-color #dfdfdf
# DarkRed + Red
set $color1 #cc241d
set $color9 #fb4934

# Window Colors
# Border Background Text Indicator
client.focused $fg-color $fg-color $bg-color $color1
client.focused_inactive #665c54 #504945 $fg-color $bg-color
client.unfocused #504945 $bg-color $fg-color $bg-color

client.urgent $color9 $color9 $fg-color $color9
client.background $bg-color
