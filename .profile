#!/bin/zsh
function addToPath {
  case ":$PATH:" in
    *":$1:"*) :;; # already there
    *) PATH="$1:$PATH";; # or PATH="$PATH:$1"
  esac
}

addToPath $HOME/.local/bin
addToPath $HOME/.local/npm/bin
addToPath $HOME/.local/apps/redshift/src
addToPath $HOME/.cargo/bin
addToPath $HOME/.rbenv/bin
addToPath $HOME/.nimble/bin
addToPath $HOME/go/bin

export EDITOR=kak
export OPENER=run-mailcap
export ANDROID_HOME=$HOME/.android-sdk

addToPath $ANDROID_HOME/emulator
addToPath $ANDROID_HOME/tools
addToPath $ANDROID_HOME/tools/bin
addToPath $ANDROID_HOME/platform-tools

# export XSECURELOCK_PASSWORD_PROMPT=disco

# Qt
# export QT_WAYLAND_FORCE_DPI=physical
# export QT_QPA_PLATFORMTHEME=gtk2
# export QT_STYLE_OVERRIDE=adwaita-dark
# export QT_AUTO_SCREEN_SCALE_FACTOR=0

# Gtk
# export GTK2_RC_FILES="$HOME/.gtkrc-2.0"

