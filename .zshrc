# source ~/.zprofile
# zmodload zsh/zprof
eval "$(starship init zsh)"

source ~/.zsh_profile_encrypted
source ~/.profile

# == HISTORY ==
HISTFILE=~/.zsh_history
HISTSIZE=50000
SAVEHIST=10000

setopt extended_history # record timestamp of command in HISTFILE
setopt hist_expire_dups_first # delete duplicates first when HISTFILE size exceeds HISTSIZE
setopt hist_ignore_dups # ignore duplicated commands history list
setopt hist_ignore_space # ignore commands that start with space
setopt hist_verify # show command with history expansion to user before running it
setopt inc_append_history # add commands to HISTFILE in order of execution
setopt share_history # share command history data
unsetopt BEEP # disable beeps

# == AUTOCOMPLETION ==
bindkey -e

fpath=(~/.zsh/zsh-completions/src $fpath)
autoload -Uz compinit
# https://gist.github.com/ctechols/ca1035271ad134841284
if [[ -n ${ZDOTDIR}/.zcompdump(#qN.mh+1) ]]; then
	compinit;
else
	compinit -C;
fi;
_comp_options+=(globdots)		# Include hidden files.

# Basic auto/tab complete:
zstyle ':completion:*' menu select
# Auto complete with case insenstivity
zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*'
# Show completion on first TAB
setopt menucomplete

# Colors
autoload -U colors
colors
setopt prompt_subst

export EDITOR=kak
# export RUSTC_WRAPPER=sccache

setopt autocd extendedglob notify

initrbenv() {
  eval "$(command rbenv init -)"
  rbenv "$@"
}

alias g=git
alias gc="git commit"
alias gcl="git clone"
alias gd="git diff"
alias gdc="git diff --cached"
alias gs="git status"
alias ga="git add"
alias gp="git push"
alias gpo="git push origin"
alias gpu="git pull"
alias gb="git branch"
alias gco="git checkout"
alias gl="git log"
alias gm="git merge"
alias gmt="git merge-to"

alias xs="sudo xbps-query -Rs"
alias xi="sudo xbps-install"
alias xr="sudo xbps-remove"
alias xu="sudo xbps-install -Su"

# alias nmtui="sudo nmtui"
alias htop="sudo htop"

alias ls="ls --color"
alias l=ls
alias la="ls -a"
alias grep="grep --color=auto"
#alias rg="rg -g yarn.lock"

alias ssh="TERM=xterm ssh"

alias ytlisten="mpv --ytdl-format=bestaudio"

alias ff=firefox

alias rm='trash-put'
alias r=rm
alias mv='mv -i'
alias cp='cp -i'

alias ...=../..
alias ....=../../..
alias .....=../../../..
alias ......=../../../../..

alias nau='nohup nautilus . >> /dev/null &'

# https://github.com/gokcehan/lf/blob/master/etc/lfcd.sh
lfcd () {
    tmp="$(mktemp)"
    lf -last-dir-path="$tmp" "$@"
    if [ -f "$tmp" ]; then
        dir="$(cat "$tmp")"
        rm -f "$tmp"
        if [ -d "$dir" ]; then
            if [ "$dir" != "$(pwd)" ]; then
                cd "$dir"
            fi
        fi
    fi
}
alias lf=lfcd
bindkey -s '^p' 'lf\n'

# . ~/.zsh/syntax-highlighting/zsh-syntax-highlighting.zsh
. ~/.zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
. ~/.zsh/alias-tips/alias-tips.plugin.zsh

autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search


# To find code: `showkey -a`

bindkey -- '^[[A' up-line-or-beginning-search
bindkey -- '^[[B' down-line-or-beginning-search

bindkey -- '^[[1;5D' backward-word
bindkey -- '^[[1;5C' forward-word

# bindkey -- '^H' backward-kill-word
# usar ^w lol

autoload -Uz bracketed-paste-magic
zle -N bracketed-paste bracketed-paste-magic
autoload -Uz url-quote-magic
zle -N self-insert url-quote-magic

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
